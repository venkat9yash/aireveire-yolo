import boto3
import boto3.s3
import sys
import os
import botocore

class S3Utils:
    def __init__(self):
        self.client = boto3.client('s3')
        self.s3 = boto3.resource('s3')

    def upload_to_s3(self,file_location, s3_bucket, s3_path):
        self.s3.meta.client.upload_file(file_location,s3_bucket,s3_path)
    
    def upload_folder_to_s3(self,folder_location, s3_bucket, s3_path): 
        for root,dirs,files in os.walk(folder_location):
            for file in files:
                full_file_path = os.path.join(root,file)
                if not os.path.isdir(full_file_path):
                    self.upload_to_s3(full_file_path,s3_bucket,os.path.join(s3_path,file))
                else:
                    print("Is Directory")
                    self.upload_folder_to_s3(full_file_path,s3_bucket,os.path.join(s3_path,file))


    def download_file(self, dist, local, bucket):
        try:
           self.s3.Bucket(bucket).download_file(dist, local)
        except botocore.exceptions.ClientError as e:
           if e.response['Error']['Code'] == "404":
              print("The object does not exist.")
           else:
               raise 
   
    def download_dir(self, dist, local, bucket):
        try:
            #self.s3.Object(bucket,dist).load()            
            if not os.path.exists(local):
                os.makedirs(local)
            response = self.client.list_objects(
                    Bucket = bucket,
                    Prefix = dist
                )
            
            for file in response['Contents']:
                # Get the file name
                name = file['Key'].rsplit('/', 1)
                # Download each file to local disk
                if '.' in name[1]:
                    self.client.download_file(bucket, file['Key'], local + '/' + name[1])
                # else:
                #     self.download_dir(file['Key'], local + '/' + name[1],bucket)
        except botocore.exceptions.ClientError as e:
            print('Object Doesnt exists in S3',e)
            exit()
