from s3_utils import S3Utils
import os
import subprocess
import argparse
import time

parser = argparse.ArgumentParser(description='Check Point Directory')
parser.add_argument('s3_checkpoint',type=str,
                    help='an integer for the accumulator')
args = parser.parse_args()
checkpoint_dir = args.s3_checkpoint

s3_utils = S3Utils()
s3_input_bucket = "aireverie-algorithms-data"
s3_output_bucket = "aireverie-algorithms-output"
base_dir = "darkflow"
input_data_dir = os.path.join(base_dir,"sample_img")
output_data_dir = os.path.join(input_data_dir,"out")
s3_input_data_path = "yolo/test_images"
s3_output_data_path = "yolo"
weights_folder = os.path.join(base_dir,"bin")
weights_file = os.path.join(weights_folder,"yolo-tiny.weights")
config_file = os.path.join(base_dir,"cfg","tiny-yolo-voc.cfg")
checkpoints_dir = os.path.join("ckpt")
ts = time.time()
#load checkpoints and weights from s3 if not provided

print("Downloading Checkpoints  data from s3")
if not os.path.exists(checkpoints_dir):
    os.makedirs(checkpoints_dir)
    s3_utils.download_dir(s3_output_data_path+"/{}".format(checkpoint_dir),checkpoints_dir,s3_output_bucket)
    s3_utils.download_file(s3_output_data_path+"/{}/checkpoint".format(checkpoint_dir),checkpoints_dir+"/checkpoint",s3_output_bucket)
#download test dataset
#Create the input data folder if it is not present
if not os.path.exists(input_data_dir):
    os.makedirs(input_data_dir)

print("Donwloading Test data set from s3")

s3_utils.download_dir(s3_input_data_path,input_data_dir,s3_input_bucket)

#Predict using the model 
prediction_call = subprocess.call('flow --imgdir {0} --model {1} --load -1 --json'.format(input_data_dir,config_file), shell=True)
if prediction_call == 0:
    #Save Predictions to S3
    print("Saving Prediction data to S3")
    if os.path.exists(output_data_dir):
        s3_utils.upload_folder_to_s3(output_data_dir,s3_output_bucket,s3_output_data_path+"/predictions_{}".format(ts))
    else:
        print("Predictions are not present to upload to S3")
else:
    print("Prediction Failed")

