from s3_utils import S3Utils
import os
import subprocess
import time

s3_utils = S3Utils()
s3_input_bucket = "aireverie-algorithms-data"
s3_output_bucket = "aireverie-algorithms-output"
base_dir = "darkflow"
input_data_dir = os.path.join(base_dir,"VOC2007")
s3_input_data_path = "yolo/train_images/VOC2007"
s3_output_data_path = "yolo"
config_file = os.path.join(base_dir,"cfg","tiny-yolo-voc.cfg")
weights_file = os.path.join(base_dir,"bin","yolo-tiny.weights")
checkpoints_dir = os.path.join("ckpt")
ts = time.time()

#Create the input data folder if it is not present
if not os.path.exists(input_data_dir):
    os.makedirs(input_data_dir)

#downloadfrom s3 
print("Downloading Annotations Training data from S3")
annotations_training_path = os.path.join(input_data_dir,"Annotations")
s3_utils.download_dir(s3_input_data_path+"/Annotations",annotations_training_path,s3_input_bucket)

print("Downloading Image Trainig data from s3")
images_training_path = os.path.join(input_data_dir,"JPEGImages")
s3_utils.download_dir(s3_input_data_path+"/JPEGImages",images_training_path,s3_input_bucket)

#train the model with the config
training_call = subprocess.call('flow --model {0} --train --dataset "{1}" --annotation "{2}" --epoch 20'.format(config_file,images_training_path,annotations_training_path), shell=True)

if training_call == 1:
    print("Uploading Checkpoints into S3")
    if os.path.exists(checkpoints_dir):
        s3_utils.upload_folder_to_s3(checkpoints_dir,s3_output_bucket,s3_output_data_path+"/checkpoints_{}".format(ts))
    else:
        print("Checkpoint directory was not found")


else:
    print("Training Failed")





